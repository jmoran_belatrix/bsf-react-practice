import React, { useState } from 'react';
import { CSSTransition } from 'react-transition-group';
import numeral from 'numeral';
/** Components */
import { 
  BoxComponent,
  ButtonComponent,
  InputComponet, 
  TextComponent 
} from './Components';
/** Styles */
import './App.scss';


function App() {
  const [startAnimate, setStartAnimate] = useState(false);
  const [inputNumber, setInputNumber] = useState('');
  const [dollarFormat, setDollarFormat] = useState('')

  const handleInputNumber = (value) => {
    setInputNumber(value);
  }

  const handleTranslateBox = (event) => {
    setStartAnimate(true);
  }

  const handleGetFormat = (event) => {
    if (!Number.isNaN(inputNumber) && inputNumber !== '') {
      const formated = numeral(inputNumber).format('$ 0,0.00');
      setDollarFormat(formated);
    } else {
      alert('Input a valid number!');
    }
  }

  return (
    <main role="main" className="App">
      <CSSTransition
        classNames="moveBox"
        in={startAnimate}
        timeout={200}
      >
        <BoxComponent />
      </CSSTransition>
      
      <section className="App-content">
        <InputComponet type="number" value={inputNumber} handleChange={handleInputNumber} />
        
        <div className="App-actions">
          <ButtonComponent handleClick={handleTranslateBox} title="Mover" />
          <ButtonComponent handleClick={handleGetFormat} title="Convertir" />
        </div>

        <TextComponent text={dollarFormat} />
      </section>
    </main>
  );
}

export default App;
