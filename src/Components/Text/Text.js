import React from 'react';
import PropTypes from 'prop-types';
import './Text.scss';

const TextComponent = ({ text }) => {
  return <label>{text}</label>
}

TextComponent.propTypes = {
  text: PropTypes.string
};

TextComponent.defaultProps = {
  text: ''
};

export default TextComponent;