import React from 'react';
import PropTypes from 'prop-types';
import './Input.scss'

const InputComponent = ({ type, value, handleChange }) => {
  const handleOnChange = (event) => {
    const { value } = event.target;
    handleChange(value)
  }

  return (
    <input 
      className="input"
      type={type}
      value={value}
      onChange={handleOnChange}
    />
  )
}

InputComponent.propTypes = {
  type: PropTypes.string,
  value: PropTypes.string,
  handleChange: PropTypes.func
};

InputComponent.defaultProps = {
  type: 'text',
  value: '0',
  handleChange: () => {}
};

export default InputComponent;