import BoxComponent from "./Box/Box";
import InputComponet from './Input/Input';
import TextComponent from './Text/Text';
import ButtonComponent from './Button/Button';

export { 
  BoxComponent,
  ButtonComponent,
  InputComponet,
  TextComponent
};