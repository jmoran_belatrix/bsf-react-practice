import React from 'react';
import PropTypes from 'prop-types';
import './Button.scss';

const ButtonComponent = ({ title, handleClick }) => {

  return (
    <button className="button" onClick={handleClick}>
      {title}
    </button>
  )
}

ButtonComponent.propTypes = {
  title: PropTypes.string,
  handleClick: PropTypes.func
};

ButtonComponent.defaultProps = {
  title: '',
  handleClick: () => {}
};

export default ButtonComponent;